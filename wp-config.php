<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'josephinekarungi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3308' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'UodTHn;#oI:F:&EM[dGrtRs/v[to20Cm=;S|O,mt!S/~tGFM3}4%i/ILw2;%ALo^' );
define( 'SECURE_AUTH_KEY',  '{EVBJLEeHf/y(_S,}5blnYm?b%x7?j{$D7!D){g&&SN*dvr8/J86C`c.9(stx/W4' );
define( 'LOGGED_IN_KEY',    'r=GK3#PmI+MO5ayw>4=7@n6}J0>q[Ejye{) rbPX`Ngd9J/.(8`6 DW&MJ{k{G1-' );
define( 'NONCE_KEY',        'zKYuhJP>dwc1~Z;KGx#Je=_2ri#{{H>HJ0AjeUX3mM2]k:EtM-NP-+az Oa(/C$>' );
define( 'AUTH_SALT',        'x/$be&#?QcnNnsXohZU-`;%_*]K,bkDx!s]]}c7sSnC`mj+uL*#(;j^?f5_j7[:Z' );
define( 'SECURE_AUTH_SALT', 'x5]+ny#&2wXKHl7DDX?rIr]&z+8:lazZ+l*$||muI@C+V#lA?>eAImD: yxt}Ex]' );
define( 'LOGGED_IN_SALT',   ',e^0`m)smXoUud&GlXuj#9/gSC<F-Q& 9(p&|vFr!IDd9-0^.8(Y`+&,xm{%~)f;' );
define( 'NONCE_SALT',       '_~,FDF/g]lzD(`, E_%t=^B{:`q?jq4oXwdd{Cv_!.=0)%.uJVI#X3%TGDeXH|E!' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
